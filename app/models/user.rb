class User < ApplicationRecord
	# Include default devise modules. Others available are:
	# :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
				 :recoverable, :rememberable, :trackable, :validatable, :confirmable

	has_many :oauth_applications, class_name: 'Doorkeeper::Application', as: :owner

	#Custom validation
	validate :presence_domain_email

	def presence_domain_email
		#if email domain not on valid list then return an error.

		valid = false

		Settings.valid_emails.each do |test_text|
			test = Regexp.new(test_text, Regexp::IGNORECASE)
			if test.match(email)
				valid = true
			end
		end

		# Generate an error if the domain is not valid
		unless valid
			errors.add(:email, 'does not match any approved condition.')
		end
	end
end
