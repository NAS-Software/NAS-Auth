module Api
	class UserInfoController < ApiController
		before_action :doorkeeper_authorize!

		def info
			render json: {email: current_user.email, id: current_user.id}
		end
	end
end
