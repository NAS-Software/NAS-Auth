module Api
	class ApiController < ActionController::API

		private
		def current_user
			@current_user ||= User.find(doorkeeper_token.resource_owner_id)
		end
	end
end
