class ApplicationController < ActionController::Base
	# API, don't need this
  # protect_from_forgery with: :exception
	protect_from_forgery except: :index
end
