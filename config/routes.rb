Rails.application.routes.draw do
	use_doorkeeper do
		controllers :applications => 'oauth/applications'
	end

  devise_for :users

	root 'pages#index'

	get 'api/info', to: 'api/user_info#info'
	
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
